module Novacast
  module SDK
    module Event
      autoload :Client, 'novacast-sdk/event/client'
    end
  end
end