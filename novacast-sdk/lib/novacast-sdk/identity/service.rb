module Novacast
  module SDK
    module Identity
      autoload :Client, 'novacast-sdk/identity/client'
    end
  end
end