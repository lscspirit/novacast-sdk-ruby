require 'novacast-sdk/apis/identity_v1/resources'
require 'novacast-sdk/apis/identity_v1/operations'

module Novacast
  module API
    module IdentityV1
      VERSION = '1'
    end
  end
end