require 'novacast-sdk/apis/event_v1/resources'
require 'novacast-sdk/apis/event_v1/operations'

module Novacast
  module API
    module EventV1
      VERSION = '1'
    end
  end
end