module Novacast
  module SDK
    autoload :Client,         'novacast-sdk-core/client'
    autoload :Operation,      'novacast-sdk-core/operation'
    autoload :Request,        'novacast-sdk-core/request'
    autoload :RequestBuilder, 'novacast-sdk-core/request_builder'
    autoload :Response,       'novacast-sdk-core/response'
    autoload :JsonRepresentation, 'novacast-sdk-core/json_representation'
  end
end